
#include <assert.h>

#include "utils.h"
#include "allocator.h"
#include "c_allocator.h"


#define maple_alloc(a, s)      ((FreeListAllocator*)a)->alloc(s)
#define maple_free(a, p)       ((FreeListAllocator*)a)->free(p)
#define maple_realloc(a, p, n) ((FreeListAllocator*)a)->realloc(p,n)

#include "red_black_tree.h"
#include "dyn_array.h"

void test_red_black_tree()
{
	int size = 4194304; // 4mb
	FreeListAllocator a;
	a.initialize_allocator(size);

	RBNode *root = (RBNode*)maple_alloc(&a, sizeof(RBNode));
	initialize_rb_tree(root, 8);
}

int main(void)
{
#ifdef DEBUG

	// test_c_dyn_array();
	free_list_internal_tests();
	segregated_list_internal_tests();

#endif // DEBUG

#if 1
	debug("\nBENCHMARKING\n");	
	debug("-------------------------------------------------------------\n");

	run_free_list_benchmark(1e3);
	run_segragated_list_benchmark(1e3);
	run_c_alloc_benchmark(1e3);
#endif 

	return (0);
}