
#ifndef RED_BLACK_TREE_H
#define RED_BLACK_TREE_H

#include <stdlib.h>

enum RBColor
{
	RB_COLOR_BLACK,
	RB_COLOR_RED,
};

// Single Node in a Red-Black Tree
struct RBNode
{
	RBNode *parent;
	RBNode *left;
	RBNode *right;

	RBColor color;
	size_t key; // experimental key. Size of the node 
};

void initialize_rb_tree(RBNode *root, size_t key);
void insert(RBNode *root, RBNode x);
void remove(RBNode *root, RBNode x);
void find(RBNode *root, size_t key);

/*
Notes on a Red-Black Tree

Rules:
1. A node is either red or black.
2. A red node must have all black children. 
3. The root and leaves (NIL) are black.
4. All paths from a node to its NIL descendants have the same number of black nodes.


// Rotation should preserve in-order traversal

left_rotate(root, h)
{
	
}

right_rotate(root, h)
{
	
}

bst_insert(root, h)
{
	
}

insert(root, h)
{
	
}

*/

#endif // RED_BLACK_TREE_H