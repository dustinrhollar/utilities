
#define MAPLE_DYN_ARRAY_IMPLEMENTATION
#define MAPLE_CLASS_DYN_IMPLEMENTATION
#include "dyn_array.h"

void *maple_initializef(void *ptr, void *allocator, size_t size_per_element, size_t cap)
{
	size_t size = DYN_HEADER_SIZE + size_per_element * cap;
	DynArrayHeader *h = (DynArrayHeader*)maple_dyn_alloc(allocator, size);
	h->a = allocator;
	h->len = 0;
	h->cap = cap;

	void *tmp = ((char*)h + DYN_HEADER_SIZE);
	return tmp;
}

void *maple_growf(void *ptr, size_t size_per_element, size_t add_len, size_t min_cap)
{
	size_t total_len = maple_arr_len(ptr) + add_len;

	if (total_len < min_cap)
		return ptr;

	if (2*maple_arr_cap(ptr) > min_cap)
		min_cap = 2*maple_arr_cap(ptr);
	else if (4 > min_cap)
		min_cap = 4;

	size_t size = min_cap * size_per_element + DYN_HEADER_SIZE;
	DynArrayHeader *b = (DynArrayHeader*)maple_dyn_realloc(maple_allocator(ptr), mem2dyn_header(ptr), size);
	
	b->a = (ptr) ? maple_allocator(ptr) : nullptr;
	b->cap = min_cap;
	b->len = (ptr) ? maple_arr_len(ptr) : 0;

	return (void*)((char*)b + DYN_HEADER_SIZE);	
}

#if defined(DEBUG)

#include <assert.h>

void test_c_dyn_array()
{
	// DynArray<int> da = new DynArray(0, nullptr, nullptr, 0);

	debug("\nTEST SUITE: Dynamic Array\n");
	debug("-------------------------------------------------------------\n");

	int size = 4194304; // 4mb
	FreeListAllocator a;
	a.initialize_allocator(size);

	debug("\tTest 1: Create Dynamic Array with capacity 5.\n");

	int *array = nullptr;
	array = (int*)arr_initialize(array, &a, sizeof(int), 5);

	assert(array != nullptr);
	assert(arr_cap(array) == 5);
	assert(arr_len(array) == 0);

	debug("\tTest 2: Add 7 elements to force a resize.\n");

	arr_put(array, 0);
	assert(arr_len(array) == 1);
	assert(array[0] == 0);

	arr_put(array, 1);
	assert(arr_len(array) == 2);
	assert(array[0] == 0);
	assert(array[1] == 1);

	arr_put(array, 2);
	assert(arr_len(array) == 3);
	assert(array[0] == 0);
	assert(array[1] == 1);
	assert(array[2] == 2);

	arr_put(array, 3);
	assert(arr_len(array) == 4);
	assert(array[0] == 0);
	assert(array[1] == 1);
	assert(array[2] == 2);
	assert(array[3] == 3);

	arr_put(array, 4);
	assert(arr_cap(array) == 5);
	assert(arr_len(array) == 5);
	assert(array[0] == 0);
	assert(array[1] == 1);
	assert(array[2] == 2);
	assert(array[3] == 3);
	assert(array[4] == 4);

	arr_put(array, 5);
	arr_put(array, 6);
	assert(arr_cap(array) == 10);
	assert(arr_len(array) == 7);
	assert(array[0] == 0);
	assert(array[1] == 1);
	assert(array[2] == 2);
	assert(array[3] == 3);
	assert(array[4] == 4);
	assert(array[5] == 5);
	assert(array[6] == 6);

	debug("\tTest 3: Remove a few elements via pop\n");
	int e = arr_pop(array);
	assert(e == 6);
	assert(arr_len(array) == 6);

	e = arr_pop(array);
	assert(e == 5);
	assert(arr_len(array) == 5);

	a.free_allocator();
}

#endif // DEBUG