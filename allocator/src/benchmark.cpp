#include "benchmark.h"

BenchmarkResults build_benchmark_results(unsigned int num_operations, double elapsed_time, size_t memory_peak)
{
	BenchmarkResults results;
	results.num_operations = num_operations;
	results.elapsed_time = elapsed_time;
	results.operations_per_sec = results.num_operations / results.elapsed_time;
	results.time_per_operation = results.elapsed_time / results.num_operations;
	results.memory_peak = memory_peak;

	return results;
}

void print_benchmark_results(BenchmarkResults *results)
{
	debug("\tOperations: %ld\n", results->num_operations);
	debug("\tElapsed Time: %lf\n", results->elapsed_time);
	debug("\tOperations per second: %f\n", results->operations_per_sec);
	debug("\tTime per operation: %f\n", results->time_per_operation);
	// debug("\tMemory Peak: %d\n", results->memory_peak);
}

void write_benchmark_header(FILE *fp)
{
	fprintf(fp, "Number of Operations, Elapsed Time, Operations per second, Timer per operation\n");
}

void write_benchmark_file(FILE *fp, BenchmarkResults *results)
{
	fprintf(fp, "%ld, %lf, %f, %f\n", results->num_operations, results->elapsed_time, results->operations_per_sec, results->time_per_operation);
}