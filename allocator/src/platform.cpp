
#include "platform.h"

#ifdef __unix__

double platform_elapsed_time(platform_time *start, platform_time *end)
{
	platform_time time;

	if (end->tv_nsec - start->tv_nsec < 0)
	{
		time.tv_sec = end->tv_sec - start->tv_sec - 1;
		time.tv_nsec = 1e9 + end->tv_nsec - start->tv_nsec;
	}
	else
	{
		time.tv_sec = end->tv_sec - start->tv_sec;
		time.tv_nsec = end->tv_nsec - start->tv_nsec;	
	}

	double time_sec = (double)time.tv_sec;
	double time_nsec = (double)time.tv_nsec;
	double time_msec = (time_sec * 1e3) + (time_nsec / 1e6);

	return time_msec;
}

#endif // unix