#ifndef MAPLE_BENCHMARK_H
#define MAPLE_BENCHMARK_H

#include "utils.h"

// Templates for Benchmark functions
#define BENCHMARK_SINGLE_ALLOCATION(name)   void name(FILE *fp, void *allocator, size_t size, size_t num_operations)
#define BENCHMARK_MULTIPLE_ALLOCATION(name) void name(void *allocator, size_t *sizes, size_t size_count, size_t num_operations)

#define BENCHMARK_SINGLE_FREE(name)   void name(FILE *fp, void *allocator, size_t size, size_t num_operations)
#define BENCHMARK_MULTIPLE_FREE(name) void name(void *allocator, size_t *sizes, size_t size_count, size_t num_operations)

struct BenchmarkResults
{
	long num_operations;
	double elapsed_time;
	float operations_per_sec;
	float time_per_operation;
	int memory_peak;
};

BenchmarkResults build_benchmark_results(unsigned int num_operations, double elapsed_time, size_t memory_peak);
void print_benchmark_results(BenchmarkResults *results);

void write_benchmark_header(FILE *fp);
void write_benchmark_file(FILE *fp, BenchmarkResults *results);

#endif // MAPLE_BENCHMARK_H