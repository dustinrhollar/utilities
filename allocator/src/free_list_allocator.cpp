#include "allocator.h"
#include "platform.h"

#define LIST_HEADER      2*MIN_LINK_SIZE
#define adjusted_size(s) ((s) >= LIST_HEADER) ? (s) : (s) + MIN_LINK_SIZE

//------------------------------------------------------//
// Arena implementation

#include <string.h>
void Arena::initialize(size_t size)
{
	size = ALIGN(size);

	arena_size = size;
	// arena = malloc(size);
	arena = os_alloc_memory(arena_size);
	if (arena == MAP_FAILED)
	{
		perror("Could not map!\n");
		return;
	}

	_brkp = (char*)arena;
	_endp = _brkp + arena_size;

	memset(arena, 0, arena_size);
	// char *iter = (char*)_endp;
	// while (iter != arena)
	// {
	// 	*iter-- = 0;
	// }
}

void Arena::free()
{
	int unmap_result = os_free_memory(arena, arena_size);
	if (unmap_result != 0)
	{
		perror("Could not unmap memory!\n");
		return;
	}

	// free(arena);

	arena = nullptr;
	_brkp = nullptr;
	arena_size = 0;
}

void *Arena::_sbrk(size_t increment)
{
	if (increment == 0)
		return (void*)_brkp;

	// make sure not to exceed allocated memory
	void *free = (void*)_brkp;

	if (_brkp + increment >= _endp)
		return NULL;

	_brkp += increment;
	return free;
}

void *Arena::request_memory(size_t size)
{
	void *memory = (header_t*)_sbrk(0);

	// Out of Memory?
	if (_sbrk(ALIGN(size)) == NULL)
	{
		return nullptr; // yep.
	}

	return memory;
}


//------------------------------------------------------//
// Heap implementation

void Heap::initialize(SearchMode sm)
{
	search_mode = sm;

	last_access = nullptr;
	free_list   = nullptr;
}

void Heap::free()
{
	last_access = nullptr;
	free_list   = nullptr;
}

void Heap::coalesce(header_t *left_header, header_t *right_header)
{
	if (!left_header || !right_header)
		return;

	// i arbitrarily decided that h1 will be the header
	// that is kept.
	if (left_header < right_header)
	{
		left_header->next = right_header->next;
		if (right_header->next) right_header->next->prev = left_header;
	}
	else
	{
		left_header->prev = right_header->prev;
		if (right_header->prev) right_header->prev->next = left_header;
	}

	size_t h1_size = adjusted_size(data_size(left_header));
	size_t h2_size = adjusted_size(data_size(right_header));
	data_set_size(left_header, h1_size + h2_size + MIN_HEADER_SIZE);
}

header_t *Heap::split(header_t *header, size_t size)
{
	if (!can_split(header, size)) return header;

	size_t adj_header_size = adjusted_size(data_size(header));
	size_t adj_size = adjusted_size(size);
	size_t full_size = MIN_HEADER_SIZE + adj_size;
	size_t leftover = adj_header_size - adj_size - MIN_HEADER_SIZE;

	// debug();

	data_set_size(header, size); // size being requested

	header_t *split_header = (header_t*)((char*)header + full_size);
	data_set_size(split_header, leftover);
	free_list_add(split_header);

	return header;
}

// free list data structure
size_t Heap::free_list_size()
{
	header_t *iter = free_list;
	int count = 0;
	while(iter != nullptr)
	{
		++count;
		iter = iter->next;
	}
	return count;
}

void Heap::free_list_add(header_t *header)
{
	// init the list
	if (free_list == nullptr)
	{
		free_list = header;
		free_list->prev = nullptr;
		free_list->next = nullptr;
		return;
	}

	header_t *iter = free_list;

	// add to the front
	if (header < free_list)
	{
		header->prev = free_list->prev;
		header->next = free_list;
		free_list = header;
		// return;
	}
	else 
	{
		header_t *iter = free_list;
		while (iter->next != nullptr && header > iter->next)
		{
			iter = iter->next;
		}

		// insert the header into the list
		header->prev = iter;
		header->next = iter->next;

		if (iter->next)
		{
			iter->next->prev = header;
		}

		iter->next = header;
	}

	// If the new node is besides already existing nodes, merge them together
	iter = header;

	// debug("Free List Add Merge Check. Current: %p. Left: %p. Right: %p.\n", iter, iter->prev, iter->next);

	// debug("Iter Size: %ld, Iter Adj Size: %ld, ");
	if (iter->next) 
	{
		size_t adj = adjusted_size(data_size(iter));
		if (((char*)iter + MIN_HEADER_SIZE + adj) == (char*)iter->next)
		{
			coalesce(iter, iter->next);
		}
	}

	if (iter->prev)
	{
		size_t adj = adjusted_size(data_size(iter->prev));
		if (((char*)iter - MIN_HEADER_SIZE - adj) == (char*)iter->prev)
		{
			coalesce(iter->prev, iter);
		}
	}
}

void Heap::free_list_remove(header_t *header)
{
	if (free_list == nullptr || header == nullptr) return;
 
	if (free_list == header)
	{
		free_list = header->next;
	}

	// Remove it from the free list
	if (header->prev != nullptr)
	{
		header->prev->next = header->next;	
	}

	if (header->next != nullptr)
	{
		header->next->prev = header->prev;
	}

	header->prev = nullptr;
	header->next = nullptr;
}

// Heap search methods
header_t *Heap::find_free_header(size_t size)
{
	header_t *header = nullptr;
	switch(search_mode)
	{
		case SEARCH_MODE_FIRST_FIT:
		{
			header = first_fit(size);
		} break;

		case SEARCH_MODE_NEXT_FIT:
		{
			header = next_fit(size);
		} break;

		case SEARCH_MODE_BEST_FIT:
		{
			header = best_fit(size);
		} break;

		default: return nullptr;
	}

	free_list_remove(header);

	return header;
}

header_t *Heap::first_fit(size_t size)
{
	header_t *header = free_list;

	int count = 0;
	while (header != nullptr)
	{
		if (!header_used(header) && size <= data_size(header))
		{
			return header;
		}

		count ++;
		header = header->next;
	}

	return nullptr;
}

header_t *Heap::next_fit(size_t size)
{
	if (free_list == nullptr) return nullptr; // no blocks
	if (last_access == nullptr) last_access = free_list; // last_access not init yet

	header_t *header = last_access;
	do
	{
		if (!header_used(header) && size <= data_size(header))
		{
			last_access = header;
			return header;
		}

		header = header->next;
		if (header == nullptr)
		{ // wrap back to the front of the list
			header = free_list;
		}

		if (header == last_access) 
		{ // stop when the last accessed element has been reached again
			return nullptr;
		}
	} while (true);
}

header_t *Heap::best_fit(size_t size)
{
	header_t *header = free_list;
	header_t *best_header = nullptr;

	while (header != nullptr)
	{
		if (!header_used(header) && size <= data_size(header))
		{
			if (best_header == nullptr)
			{
				best_header = header;
			}
			else 
			{
				// guaranteed that header greater or equal to the desired size
				// therefore want to look for smaller headers that are a closer fit.
				if (data_size(header) < data_size(best_header))
				{
					best_header = header;
				}
			}
		}

		header = header->next;
	}

	return best_header;
}

//------------------------------------------------------//
// FreeListAllocator implementation

void FreeListAllocator::initialize_allocator(size_t size, SearchMode search_mode)
{
	arena.initialize(size);
	heap.initialize(search_mode);
}

void FreeListAllocator::reset()
{
	size_t arena_size = arena.arena_size;
	SearchMode mode = heap.search_mode;
	heap.free();
	arena.free();

	arena.initialize(arena_size);
	heap.initialize(mode);
}

void FreeListAllocator::free_allocator()
{
	heap.free();
	arena.free();
}

void *FreeListAllocator::alloc(size_t size)
{
	size = ALIGN(size);
	size_t adj_size = adjusted_size(size);
	size_t actual_size = MIN_HEADER_SIZE + adj_size;

	// Search for an available header
	if (header_t* header = heap.find_free_header(size))
	{
		heap.split(header, size);
		header_toggle_1(header);

		return header2mem(header);
	}

	// header was not found, request from OS 
	header_t *header = (header_t*)arena.request_memory(actual_size);
	if (header == nullptr)
		return nullptr;

	data_set_size(header, size);
	header_toggle_1(header);
	header->next = nullptr;
	header->prev = nullptr;

	return header2mem(header);
}

void FreeListAllocator::copy(void *dst, void *src, size_t size)
{
	if (data_size(mem2header(dst)) < size || data_size(mem2header(src)) < size)
	{
		perror("Invalid size during memory copy! Size is larger than the destination or source.\n");
		return;
	} 
 
	char *d = (char*)dst;
	char *s = (char*)src;

	for (size_t i = 0; i < size; ++i)
	{
		*(d+i) = *(s+i);
	}
}

void *FreeListAllocator::realloc(void *ptr, size_t size)
{
	// no need to copy memory if the 
	if (data_size(mem2header(ptr)) >= size) return ptr;

	void *new_ptr = alloc(size);
	
	copy(new_ptr, ptr, data_size(mem2header(ptr)));
	free(ptr);

	return new_ptr;
}

void FreeListAllocator::free(void *data)
{
	header_t *header = (header_t*)mem2header(data);
	header_toggle_0(header);
	heap.free_list_add(header);
}

//------------------------------------------------------//
// Everything after this point is testing

BENCHMARK_SINGLE_ALLOCATION(free_list_single_allocation)
{
	FreeListAllocator *a = (FreeListAllocator*)allocator;
	Time start;
	Time end;

	// a->reset();

	set_timer(&start);

	unsigned int op = 0;
	while (op < num_operations)
	{
		a->alloc(size);
		++op;
	}
	set_timer(&end);

	double et = elapsed_time(&start, &end);
	BenchmarkResults results = build_benchmark_results(num_operations, et, 0);
	// print_benchmark_results(&results);
	write_benchmark_file(fp, &results);
}

BENCHMARK_MULTIPLE_ALLOCATION(free_list_multiple_allocation)
{
	for (size_t i = 0; i < size_count; ++i)
	{
		char buffer[128];
		sprintf(buffer, "benchmarks/free_list_alloc_benchmark_%ld.csv", sizes[i]);

		FILE *file = fopen(buffer, "w");
		write_benchmark_header(file);

		debug("\tMultiple Allocations: %ldbyte\n", sizes[i]);
		
		for (int op = 0; op < num_operations; ++op) {
			free_list_single_allocation(file, allocator, sizes[i], op);
		}
	}
}

BENCHMARK_SINGLE_FREE(free_list_single_free)
{
	FreeListAllocator *a = (FreeListAllocator*)allocator;
	Time start;
	Time end;

	// a->reset();
	int **ptr_array = (int**)a->alloc(num_operations * sizeof(int*));

	unsigned int op = 0;
	while (op < num_operations)
	{
		*(ptr_array + op) = (int*)a->alloc(size);
		++op;
	}

	set_timer(&start);

	op = 0;
	while (op < num_operations)
	{
		a->free(*(ptr_array + op));
		++op;
	}

	set_timer(&end);

	double et = elapsed_time(&start, &end);
	BenchmarkResults results = build_benchmark_results(num_operations, et, 0);
	// print_benchmark_results(&results);
	write_benchmark_file(fp, &results);
}


BENCHMARK_MULTIPLE_FREE(free_list_multiple_frees)
{
	for (size_t i = 0; i < size_count; ++i)
	{
		// We want to write to a file for each size of benchmark
		char buffer[128];
		sprintf(buffer, "benchmarks/free_list_free_benchmark_%ld.csv", sizes[i]);

		FILE *file = fopen(buffer, "w");
		write_benchmark_header(file);

		debug("\tMultiple Frees: %ldbyte\n", sizes[i]);

		for (int op = 0; op < num_operations; ++op)
			free_list_single_free(file, allocator, sizes[i], op);

		fclose(file);
	}
}

// void fl_multiple_allocation_benchmarks(size_t num_operations)
// {
// 	FreeListAllocator allocator;
// 	allocator.initialize_allocator(1e8);

// 	size_t allocation_size[] = {32, 64, 256, 512, 1024, 2048, 4096};

// 	debug("  Multiple Allocations: 32byte\n");

// 	FILE *fp = fopen("benchmarks/free_list_benchmark_32.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		free_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[0], 1, op+1);
// 	}

// 	fclose(fp);

// 		debug("  Multiple Allocations: 64byte\n");

// 	fp = fopen("benchmarks/free_list_benchmark_64.csv", "w");
// 	write_benchmark_header(fp);	

// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		free_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[1], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 256byte\n");
	
// 	fp = fopen("benchmarks/free_list_benchmark_256.csv", "w");
// 	write_benchmark_header(fp);	
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		free_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[2], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 512byte\n");

// 	fp = fopen("benchmarks/free_list_benchmark_512.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		free_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[3], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 1024byte\n");

// 	fp = fopen("benchmarks/free_list_benchmark_1024.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		free_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[4], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 2048byte\n");

// 	fp = fopen("benchmarks/free_list_benchmark_2048.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		free_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[5], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 4096byte\n");

// 	fp = fopen("benchmarks/free_list_benchmark_4096.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		free_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[6], 1, op+1);
// 	}

// 	fclose(fp);
// }

#include "dyn_array.h"
void run_free_list_benchmark(size_t num_operations, char *filename)
{
	debug("FREE LIST ALLOCATOR\n");

	FreeListAllocator allocator;
	allocator.initialize_allocator(1e8);
	
	size_t allocation_size[] = {32, 64, 256, 512, 1024, 2048, 4096};

	free_list_multiple_allocation(&allocator, allocation_size, 7, num_operations);

	allocator.reset();

	free_list_multiple_frees(&allocator, allocation_size, 7, num_operations);
	
}

#if defined(DEBUG)

#include <assert.h>

// Test Functions
inline void test_align();
inline void test_header_flags();
inline void test_alloc();
inline void test_free_list();
inline void test_next_fit();
inline void test_best_fit();
inline void test_copy();
inline void test_realloc();

void free_list_internal_tests()
{
	debug("\nRUNNING FREE LIST ALLOCATOR INTERNAL TEST SUITE....\n");
	debug("-------------------------------------------------------------\n");


	debug("TESTING: Align\n\n");
	test_align();

	debug("TESTING: Header FLAGS\n");
	test_header_flags();
	debug("\n");

	debug("TESTING: Alloc with First Fit as Default\n");
	test_alloc();	
	debug("\n");

	debug("TESTING: Free List\n");
	test_free_list();
	debug("\n");

	debug("TESTING: Next Fit\n");
	test_next_fit();	
	debug("\n");

	debug("TESTING: Best Fit\n");
	test_best_fit();	
	printf("\n");

	debug("TESTING: Copy\n");
	test_copy();	
	debug("\n");

	debug("TESTING: Realloc\n");
	test_realloc();	
	debug("\n");
}

void test_align()
{
	assert(ALIGN(3) == 8);
	assert(ALIGN(8) == 8);
	assert(ALIGN(12) == 16);
	assert(ALIGN(16) == 16);
}

void test_header_flags()
{
	header_t b;
	b.flags = 0;

	debug("\tTest 1: Set and Toggle used flag\n");
	assert(0 == header_used(&b));
	
	header_toggle_1(&b);
	assert(1 == header_used(&b));

	header_toggle_0(&b);
	assert(0 == header_used(&b));

	debug("\tTest 2: Set size\n");
	assert(data_size(&b) == 0);

	data_set_size(&b, 100);
	assert(data_size(&b) == 100);
	assert(0 == header_used(&b));

	debug("\tTest 3: Toggle used after size has been set\n");
	header_toggle_1(&b);
	assert(data_size(&b) == 100);
	assert(1 == header_used(&b));

	header_toggle_0(&b);
	assert(data_size(&b) == 100);
	assert(0 == header_used(&b));

	debug("\tTest 4: Can a header split?\n");
	data_set_size(&b, 128); // 128 byte allocation
	assert(can_split(&b, 128) == false); // 0 leftover
	assert(can_split(&b, 120) == false); // 8 
	assert(can_split(&b, 112) == false); // 16
	assert(can_split(&b, 104) == false); // 24
	assert(can_split(&b, 96) == true);   // 32
}

inline void test_alloc()
{
	size_t size = 4194304;
	FreeListAllocator allocator;

	// Test 1: Should alloc good
	debug("\tTest 1: Should alloc good\n");
	allocator.initialize_allocator(size);
	allocator.heap.search_mode = (SEARCH_MODE_FIRST_FIT);


	void *p = nullptr;
	assert(p = allocator.alloc(419430)); // good
	header_t *ph = (header_t*)mem2header(p);
	assert(header_used(ph) == 1); // 1 is used, 0 is unused
	allocator.free_allocator();

	// Test 2: Exact size, should pass 
	debug("\tTest 2: Exact size, should fail \n");
	allocator.initialize_allocator(size);
	assert(allocator.alloc(size) == nullptr); // bad
	allocator.free_allocator();

	// // Test 3: Greater size, should fail 
	debug("\tTest 3: Greater size, should fail \n");
	allocator.initialize_allocator(size);
	assert(allocator.alloc(4194305) == nullptr); // bad
	allocator.free_allocator();

	// // Test 4: Multiple allocs within size
	debug("\tTest 4: Multiple allocs within size\n");
	allocator.initialize_allocator(size);
	assert(allocator.alloc(1) != nullptr); // good
	assert(allocator.alloc(12) != nullptr); // good
	assert(allocator.alloc(17) != nullptr); // good
	assert(allocator.alloc(25) != nullptr); // good
	assert(allocator.alloc(33) != nullptr); // good
	assert(allocator.alloc(1) != nullptr); // good
	assert(allocator.alloc(1) != nullptr); // good
	assert(allocator.alloc(1) != nullptr); // good
	assert(allocator.alloc(1) != nullptr); // good
	allocator.free_allocator();
	
	// // Test 5: Multiple allocs that exceed size
	debug("\tTest 5: Multiple allocs that exceed size\n");
	allocator.initialize_allocator(size);
	assert(allocator.alloc(1) != nullptr); // good
	assert(allocator.alloc(size) == nullptr); // bad
	allocator.free_allocator();

	// // Test 6: Header
	debug("\tTest 6: Header\n");
	allocator.initialize_allocator(size);
	
	void *d1 = allocator.alloc(3);
	header_t* d1h = (header_t*)mem2header(d1);
	assert(data_size(d1h) == BLOCK_SIZE);
	
	void *d2 = allocator.alloc(12);
	header_t* d2h = (header_t*)mem2header(d2);
	assert(data_size(d2h) == 16);
	
	// make sure d1 has not been overwritten
	d1h = (header_t*)mem2header(d1);
	assert(data_size(d1h) == 8);

	debug("\tTest 7: Free an object\n");
	allocator.free(d1);
	assert(header_used(d1h) == 0);
	assert(allocator.heap.free_list_size() == 1);
	assert(data_size(d2h) == 16);

	debug("\tTest 8: Re-allocate an object after first has been freed\n");
	void *d3 = allocator.alloc(8);
	header_t *d3h = (header_t*)mem2header(d3);
	assert(d1h == d3h);
	assert(allocator.heap.free_list_size() == 0);

	allocator.free(d2);
	assert(allocator.heap.free_list_size() == 1);

	assert(header_used(d2h) == 0);
	void *d4 = allocator.alloc(16);
	assert(allocator.heap.free_list_size() == 0);
	header_t *d4h = (header_t*)mem2header(d4);
	assert(d2h == d4h);
	assert(header_used(d4h) == 1);

	allocator.free_allocator();
}

inline void test_free_list()
{
	debug("\tTest 1: Alloc then free then alloc same size.\n");
	int size = 4194304; // 4mb
	FreeListAllocator allocator;

	allocator.initialize_allocator(size);

	void *p1 = allocator.alloc(8);
	allocator.free(p1);
	assert(allocator.heap.free_list_size() == 1);
	p1 = allocator.alloc(8);
	assert(allocator.heap.free_list_size() == 0);

	debug("\tTest 2: Alloc another block and then free 2 without merging.\n");
	void *p2 = allocator.alloc(8);
	void *p3 = allocator.alloc(8);
	allocator.free(p1);
	assert(allocator.heap.free_list_size() == 1);
	allocator.free(p3);
	assert(allocator.heap.free_list_size() == 2);
	
	debug("\tTest 3: Free the 4th header, triggering a merge between the 3rd and 4th headers.\n");
	void *p4 = allocator.alloc(8);
	allocator.free(p4);
	assert(allocator.heap.free_list_size() == 2);

	debug("\tTest 4: Free the 2nd header, triggering a merge between between all headers.\n");
	allocator.free(p2);
	assert(allocator.heap.free_list_size() == 1);

	allocator.free_allocator();

	allocator.initialize_allocator(size);
	debug("\tTest 5: Split test: Alloc 128 bytes, no split with 128 byte alloc.\n");
	
	p1 = allocator.alloc(128);
	allocator.free(p1);
	assert(allocator.heap.free_list_size() == 1);
	assert(data_size(allocator.heap.free_list) == 128);

	p2 = allocator.alloc(128);
	header_t *p2h = mem2header(p2);
	assert(data_size(p2h) == 128);
	assert(allocator.heap.free_list_size() == 0);

	debug("\tTest 6: Split test: Alloc 128 bytes, no split with 120 byte alloc.\n");
	
	allocator.free(p2);
	assert(allocator.heap.free_list_size() == 1);
	assert(data_size(allocator.heap.free_list) == 128);

	p2 = allocator.alloc(120);
	p2h = mem2header(p2);
	assert(data_size(p2h) == 128);
	assert(allocator.heap.free_list_size() == 0);

	debug("\tTest 7: Split test: Alloc 128 bytes, no split with 112 byte alloc.\n");
	
	allocator.free(p2);
	assert(allocator.heap.free_list_size() == 1);
	assert(data_size(allocator.heap.free_list) == 128);

	p2 = allocator.alloc(112);
	p2h = mem2header(p2);
	assert(data_size(p2h) == 128);
	assert(allocator.heap.free_list_size() == 0);

	debug("\tTest 8: Split test: Alloc 128 bytes, no split with 104 byte alloc.\n");	
	
	allocator.free(p2);
	assert(allocator.heap.free_list_size() == 1);
	assert(data_size(allocator.heap.free_list) == 128);

	p2 = allocator.alloc(104);
	p2h = mem2header(p2);
	assert(data_size(p2h) == 128);
	assert(allocator.heap.free_list_size() == 0);

	debug("\tTest 9: Split test: Alloc 128 bytes, split with 96 byte alloc.\n");
	
	allocator.free(p2);
	assert(allocator.heap.free_list_size() == 1);
	assert(data_size(allocator.heap.free_list) == 128);
	
	p2 = allocator.alloc(96);
	p2h = mem2header(p2);
	assert(allocator.heap.free_list_size() == 1);
	assert(data_size(p2h) == 96);
	assert(data_size(allocator.heap.free_list) == 24);

	allocator.free(p2);
	assert(allocator.heap.free_list_size() == 1);
	assert(data_size(allocator.heap.free_list) == 128);


	allocator.free_allocator();
}

inline void test_next_fit()
{
	int size = 4194304; // 4mb
	FreeListAllocator allocator;

	allocator.initialize_allocator(size);
	allocator.heap.search_mode = (SEARCH_MODE_NEXT_FIT);

    //   ls
	// [8, 1], [8, 1], [8, 1], [8, 1]
	allocator.alloc(8);
	assert(nullptr == allocator.heap.last_access);

	void *p2 = allocator.alloc(8);
	header_t  *b2 = mem2header(p2);
	assert(allocator.heap.free_list == allocator.heap.last_access);

	void *p3 = allocator.alloc(8);
	header_t  *b3 = mem2header(p3);
	assert(allocator.heap.free_list == allocator.heap.last_access);

	void *p4 = allocator.alloc(8);
	header_t  *b4 = mem2header(p4);
	assert(allocator.heap.free_list == allocator.heap.last_access);

    //   ls
	// [8, 1], [8, 1], [8, 0], [8, 1]
	allocator.free(p3);

	//                   ls
	// [8, 1], [8, 1], [8, 1], [8, 1]
	debug("\tTEST 1: Remove 3rd element and insert another in its place.\n");
	void *p5 = allocator.alloc(8);
	header_t  *b5 = mem2header(p5);
	assert(b5 == b3);
	assert(allocator.heap.last_access == b5);

	//                   ls
	// [8, 1], [8, 0], [8, 1], [8, 0]
	assert(allocator.heap.free_list_size() == 0);
	allocator.free(p2);
	allocator.free(p4);
	assert(allocator.heap.free_list_size() == 2);
	

	//           ls                
	// [8, 1], [8, 1], [8, 1], [8, 0]
	debug("\tTEST 2: Remove 2nd and 4th. Insert another memory. Should be at 4th loc\n");
	void *p6 = allocator.alloc(8);
	header_t  *b6 = mem2header(p6);
	assert(allocator.heap.free_list_size() == 1);
	assert(allocator.heap.last_access == b2);
	assert(b6 == b2);

	//                           ls
	// [8, 1], [8, 1], [8, 1], [8, 0]
	debug("\tTEST 3: Cont. Test 2. Insert another. Should be at 2nd loc.\n");
	void *p7 = allocator.alloc(8);
	header_t  *b7 = mem2header(p7);
	assert(allocator.heap.free_list_size() == 0);
	assert(b7 == b4);
	assert(allocator.heap.last_access == b7);

	allocator.free_allocator();
}

inline void test_best_fit()
{
	int size = 4194304; // 4mb
	FreeListAllocator allocator;

	allocator.initialize_allocator(size);
	allocator.heap.search_mode = (SEARCH_MODE_BEST_FIT);

	// Create the list:
	// [32, 1], [8, 1], [16, 1], [8, 1], [8, 1]
	// need to filler between every other node to prevent merging
	void *p1 = allocator.alloc(32);
	header_t  *b1 = mem2header(p1);

	allocator.alloc(8);

	void *p2 = allocator.alloc(16);
	header_t  *b2 = mem2header(p2);

	allocator.alloc(8);

	void *p3 = allocator.alloc(8);
	header_t  *b3 = mem2header(p3);

	assert(data_size(b1) == 32);
	assert(data_size(b2) == 16);
	assert(data_size(b3) == 8);

	printf("\tTEST 1: Given a free list of [32, 0], [16, 0], [8, 0], alloc 8 bytes then 16 bytes then 32 bytes.\n");

	// [32, 0], [8, 1], [16, 0], [8, 1], [8, 0]
	allocator.free(p1);
	allocator.free(p2);
	allocator.free(p3);

	assert(header_used(b1) == 0);
	assert(header_used(b2) == 0);
	assert(header_used(b3) == 0);

	void *p4 = allocator.alloc(8);
	header_t  *b4 = mem2header(p4);
	assert(b4 == b3);

	void *p5 = allocator.alloc(16);
	header_t  *b5 = mem2header(p5);
	assert(b5 == b2);

	void *p6 = allocator.alloc(32);
	header_t  *b6 = mem2header(p6);
	assert(b6 == b1);

	allocator.free_allocator();
}

inline void test_copy()
{
	int size = 4194304; // 4mb

	FreeListAllocator allocator;
	allocator.initialize_allocator(size);
	allocator.heap.search_mode = (SEARCH_MODE_FIRST_FIT);

	// Test 1: Should alloc good
	debug("\tTest 1: Allocate an array of 5 ints and copy it to another array of same size.\n");

	// an array of 5 ints
	int *ptr = (int*)allocator.alloc(5 * sizeof(int));
	ptr[0] = 0;
	ptr[1] = 3;
	ptr[2] = 5;
	ptr[3] = 6;
	ptr[4] = 9;

	int *cpy = (int*)allocator.alloc(5 * sizeof(int));
	allocator.copy(cpy, ptr, 5*sizeof(int));

	assert(ptr[0] == cpy[0]);
	assert(ptr[1] == cpy[1]);
	assert(ptr[2] == cpy[2]);
	assert(ptr[3] == cpy[3]);
	assert(ptr[4] == cpy[4]);

	allocator.free_allocator();
}

inline void test_realloc()
{
	int size = 4194304; // 4mb
	FreeListAllocator allocator;
	allocator.initialize_allocator(size);
	allocator.heap.search_mode = (SEARCH_MODE_FIRST_FIT);

	// Test 1: Should alloc good
	debug("\tTest 1: Allocate an array of 5 ints and realloc it to an array of 10 ints.\n");

	int *ptr = (int*)allocator.alloc(5 * sizeof(int));
	ptr[0] = 0;
	ptr[1] = 3;
	ptr[2] = 5;
	ptr[3] = 6;
	ptr[4] = 9;

	int *new_alloc = (int*)allocator.realloc(ptr, 2*5*sizeof(int));
	assert(data_size(mem2header(new_alloc)) == 2*5*sizeof(int));
	assert(new_alloc[0] == 0);
	assert(new_alloc[1] == 3);
	assert(new_alloc[2] == 5);
	assert(new_alloc[3] == 6);
	assert(new_alloc[4] == 9);


	allocator.free_allocator();
}

#endif // DEBUG