#include "allocator.h"
#include "platform.h"

#include <string.h>

#define BIN_0 8
#define BIN_1 16
#define BIN_2 32
#define BIN_3 64
#define BIN_4 128
#define BIN_5 256
#define SEG_BIN_COUNT 7

// this seg list will not coalesce right now
#define SEG_LIST_HEADER      2*MIN_LINK_SIZE
#define adjusted_size(s) ((s) >= SEG_LIST_HEADER) ? (s) : (s) + MIN_LINK_SIZE

inline int seg_idx(size_t size)
{
	if (size <= BIN_0)
		return 0;
	else if (size <= BIN_1)
		return 1;
	else if (size <= BIN_2)
		return 2;
	else if (size <= BIN_3)
		return 3;
	else if (size <= BIN_4)
		return 4;
	else if (size <= BIN_5)
		return 5;
	else 
		return 6;
}

//------------------------------------------------------//
// Arena implementation

void *SegAllocator::_sbrk(size_t increment)
{
	if (increment == 0)
		return (void*)_brkp;

	// make sure not to exceed allocated memory
	void *free = (void*)_brkp;

	if (_brkp + increment >= _endp)
		return NULL;

	_brkp += increment;
	return free;
}

void *SegAllocator::request_memory(size_t size)
{
	void *memory = (header_t*)_sbrk(0);

	// Out of Memory?
	if (_sbrk(ALIGN(size)) == NULL)
	{
		return nullptr; // yep.
	}

	return memory;
}

//------------------------------------------------------//
// Heap implementation

void SegAllocator::coalesce(header_t *left_header, header_t *right_header)
{
	if (!left_header || !right_header)
		return;

	// i arbitrarily decided that h1 will be the header
	// that is kept.
	if (left_header < right_header)
	{
		left_header->next = right_header->next;
		if (right_header->next) right_header->next->prev = left_header;
	}
	else
	{
		left_header->prev = right_header->prev;
		if (right_header->prev) right_header->prev->next = left_header;
	}

	size_t h1_size = adjusted_size(data_size(left_header));
	size_t h2_size = adjusted_size(data_size(right_header));
	data_set_size(left_header, h1_size + h2_size + MIN_HEADER_SIZE);
}

header_t *SegAllocator::split(header_t *header, size_t size)
{
	if (!can_split(header, size)) return header;

	size_t adj_header_size = adjusted_size(data_size(header));
	size_t adj_size = adjusted_size(size);
	size_t full_size = MIN_HEADER_SIZE + adj_size;
	size_t leftover = adj_header_size - adj_size - MIN_HEADER_SIZE;

	data_set_size(header, size); // size being requested

	header_t *split_header = (header_t*)((char*)header + full_size);
	data_set_size(split_header, leftover);
	seg_list_add(&seg_list[seg_idx(leftover)], split_header);

	return header;
}

// free list data structure
size_t SegAllocator::seg_list_size(header_t *list_head)
{
	header_t *iter = list_head;
	int count = 0;
	while(iter != nullptr)
	{
		++count;
		iter = iter->next;
	}
	return count;
}

void SegAllocator::seg_list_add(header_t **list_head, header_t *header)
{
	// init the list
	if ((*list_head) == nullptr)
	{
		*list_head = header;
		(*list_head)->prev = nullptr;
		(*list_head)->next = nullptr;

		return;
	}

	header_t *iter = (*list_head);

	// add to the front
	if (header < (*list_head))
	{
		header->prev = (*list_head)->prev;
		header->next = (*list_head);
		(*list_head) = header;
	}
	else 
	{
		header_t *iter = (*list_head);
		while (iter->next != nullptr && header > iter->next)
		{
			iter = iter->next;
		}

		// insert the header into the list
		header->prev = iter;
		header->next = iter->next;

		if (iter->next)
		{
			iter->next->prev = header;
		}

		iter->next = header;
	}

	// If the new node is besides already existing nodes, merge them together
	iter = header;

	if (iter->next) 
	{
		size_t adj = adjusted_size(data_size(iter));
		if (((char*)iter + MIN_HEADER_SIZE + adj) == (char*)iter->next)
		{
			coalesce(iter, iter->next);
		}
	}

	if (iter->prev)
	{
		size_t adj = adjusted_size(data_size(iter->prev));
		if (((char*)iter - MIN_HEADER_SIZE - adj) == (char*)iter->prev)
		{
			coalesce(iter->prev, iter);
		}
	}
}

void SegAllocator::seg_list_remove(header_t **list_head, header_t **header)
{
	if (*list_head == nullptr || *header == nullptr) return;
 
	if (*list_head == *header)
	{
		*list_head = (*header)->next;
	}

	// Remove it from the free list
	if ((*header)->prev != nullptr)
	{
		(*header)->prev->next = (*header)->next;	
	}

	if ((*header)->next != nullptr)
	{
		(*header)->next->prev = (*header)->prev;
	}

	(*header)->prev = nullptr;
	(*header)->next = nullptr;
}

// Heap search methods
header_t *SegAllocator::find_free_header(size_t size)
{
	header_t **list_head = &seg_list[seg_idx(size)];

	if (header_t *header = first_fit(*list_head, size))
	{
		seg_list_remove(list_head, &header);

		return header;
	}
	else return nullptr;
}

header_t *SegAllocator::first_fit(header_t *head, size_t size)
{
	header_t *header = head;

	int count = 0;
	// necessary if the bin we are looking in is greater than 512 bytes
	while (header != nullptr)
	{
		if (!header_used(header) && size <= data_size(header))
		{
			return header;
		}

		count ++;
		header = header->next;
	}

	return nullptr;
}

//------------------------------------------------------//
// SegAllocator implementation

void SegAllocator::initialize_allocator(size_t size, SearchMode sm)
{
	arena_size = ALIGN(size);
	search_mode = sm;

	reset();
}

void SegAllocator::reset()
{
	// Clear everything - if nencessary
	if (arena)
	{
		memset(arena, 0, arena_size);

		_brkp = (char*)arena;
	}
	else 
	{
		// Setup the arena memory
		arena = os_alloc_memory(arena_size);
		if (arena == MAP_FAILED)
		{
			perror("Could not map!\n");
			return;
		}

		_brkp = (char*)arena;
		_endp = _brkp + arena_size;

		memset(arena, 0, arena_size);
	}

	// manually sets memory to 0
	// char *iter = (char*)_endp;
	// while (iter != arena)
	// {
	// 	*iter-- = 0;
	// }

	// Setup heap memory
	for (int  i = 0; i < SEG_BIN_COUNT; ++i)
	{
		seg_list[i] = nullptr;
	}
}

void SegAllocator::free_allocator()
{
	if (arena)
	{
		for (int i = 0; i < SEG_BIN_COUNT; ++i)
		{
			seg_list[i] = nullptr;
		}

		int unmap_result = os_free_memory(arena, arena_size);
		if (unmap_result != 0)
		{
			perror("Could not unmap memory!\n");
			return;
		}

		arena = nullptr;
		_brkp = nullptr;
	}	
}

void *SegAllocator::alloc(size_t size)
{
	size = ALIGN(size);
	size_t adj_size = adjusted_size(size);
	size_t actual_size = MIN_HEADER_SIZE + adj_size;

	// Search for an available header
	if (header_t* header = find_free_header(size))
	{
		split(header, size);
		header_toggle_1(header);

		return header2mem(header);
	}

	// header was not found, request from OS 
	header_t *header = (header_t*)request_memory(actual_size);
	if (header == nullptr)
		return nullptr;

	data_set_size(header, size);
	header_toggle_1(header);
	header->next = nullptr;
	header->prev = nullptr;

	return header2mem(header);
}

void SegAllocator::copy(void *dst, void *src, size_t size)
{
	if (data_size(mem2header(dst)) < size || data_size(mem2header(src)) < size)
	{
		perror("Invalid size during memory copy! Size is larger than the destination or source.\n");
		return;
	} 
 
	char *d = (char*)dst;
	char *s = (char*)src;

	for (size_t i = 0; i < size; ++i)
	{
		*(d+i) = *(s+i);
	}
}

void *SegAllocator::realloc(void *ptr, size_t size)
{
	// no need to copy memory if the 
	if (data_size(mem2header(ptr)) >= size) return ptr;

	void *new_ptr = alloc(size);
	
	copy(new_ptr, ptr, data_size(mem2header(ptr)));
	free(ptr);

	return new_ptr;
}

void SegAllocator::free(void *data)
{
	header_t *header = (header_t*)mem2header(data);
	header_toggle_0(header);

	seg_list_add(&seg_list[seg_idx(data_size(header))], header);
}

// Benchmarking Seg Allocator
//---------------------------------------------------------------------------------------

BENCHMARK_SINGLE_ALLOCATION(seg_list_single_allocation)
{
	SegAllocator *a = (SegAllocator*)allocator;
	Time start;
	Time end;

	// a->reset();

	set_timer(&start);

	unsigned int op = 0;
	while (op < num_operations)
	{
		a->alloc(size);
		++op;
	}
	set_timer(&end);

	double et = elapsed_time(&start, &end);
	BenchmarkResults results = build_benchmark_results(num_operations, et, 0);
	// print_benchmark_results(&results);
	write_benchmark_file(fp, &results);
}

BENCHMARK_MULTIPLE_ALLOCATION(seg_list_multiple_allocation)
{
	for (size_t i = 0; i < size_count; ++i)
	{
		char buffer[128];
		sprintf(buffer, "benchmarks/seg_list_alloc_benchmark_%ld.csv", sizes[i]);

		FILE *file = fopen(buffer, "w");
		write_benchmark_header(file);

		debug("\tMultiple Allocations: %ldbyte\n", sizes[i]);

		for (int op = 0; op < num_operations; ++op)
			seg_list_single_allocation(file, allocator, sizes[i], op);
	}
}

BENCHMARK_SINGLE_FREE(seg_list_single_free)
{
	SegAllocator *a = (SegAllocator*)allocator;
	Time start;
	Time end;

	// a->reset();
	int **ptr_array = (int**)a->alloc(num_operations * sizeof(int*));

	unsigned int op = 0;
	while (op < num_operations)
	{
		*(ptr_array + op) = (int*)a->alloc(size);
		++op;
	}

	set_timer(&start);

	op = 0;
	while (op < num_operations)
	{
		a->free(*(ptr_array + op));
		++op;
	}

	set_timer(&end);

	double et = elapsed_time(&start, &end);
	BenchmarkResults results = build_benchmark_results(num_operations, et, 0);
	// print_benchmark_results(&results);
	write_benchmark_file(fp, &results);
}


BENCHMARK_MULTIPLE_FREE(seg_list_multiple_free)
{
	for (size_t i = 0; i < size_count; ++i)
	{
		// We want to write to a file for each size of benchmark
		char buffer[128];
		sprintf(buffer, "benchmarks/seg_list_free_benchmark_%ld.csv", sizes[i]);

		FILE *file = fopen(buffer, "w");
		write_benchmark_header(file);

		debug("\tMultiple Frees: %ldbyte\n", sizes[i]);
		
		for (int op = 0; op < num_operations; ++op)
			seg_list_single_free(file, allocator, sizes[i], op);

		fclose(file);
	}
}

// void sl_multiple_allocation_benchmarks(size_t num_operations)
// {
// 	SegAllocator allocator;
// 	allocator.initialize_allocator(1e8);

// 	size_t allocation_size[] = {32, 64, 256, 512, 1024, 2048, 4096};

// 	debug("FREE LIST ALLOCATOR\n");
// 	debug("  Multiple Allocations: 32byte\n");

// 	FILE *fp = fopen("benchmarks/seg_list_benchmark_32.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		seg_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[0], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 64byte\n");

// 	fp = fopen("benchmarks/seg_list_benchmark_64.csv", "w");
// 	write_benchmark_header(fp);	

// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		seg_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[1], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 256byte\n");
	
// 	fp = fopen("benchmarks/seg_list_benchmark_256.csv", "w");
// 	write_benchmark_header(fp);	
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		seg_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[2], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 512byte\n");

// 	fp = fopen("benchmarks/seg_list_benchmark_512.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		seg_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[3], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 1024byte\n");

// 	fp = fopen("benchmarks/seg_list_benchmark_1024.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		seg_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[4], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 2048byte\n");

// 	fp = fopen("benchmarks/seg_list_benchmark_2048.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		seg_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[5], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 4096byte\n");

// 	fp = fopen("benchmarks/seg_list_benchmark_4096.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		seg_list_multiple_allocation(fp, (void*)&allocator, &allocation_size[6], 1, op+1);
// 	}

// 	fclose(fp);
// }

void run_segragated_list_benchmark(size_t num_operations)
{
	debug("SEGREGATED LIST ALLOCATOR\n");

	FreeListAllocator allocator;
	allocator.initialize_allocator(1e8);
	
	size_t allocation_size[] = {32, 64, 256, 512, 1024, 2048, 4096};

	seg_list_multiple_allocation(&allocator, allocation_size, 7, num_operations);

	allocator.reset();

	seg_list_multiple_free(&allocator, allocation_size, 7, num_operations);

}

// Testing Seg Allocator
//---------------------------------------------------------------------------------------
#ifdef DEBUG

#include <assert.h>

inline void test_seg_alloc();
inline void test_seg_list();
inline void test_seg_copy();
inline void test_seg_realloc();

void segregated_list_internal_tests()
{
	debug("\nRUNNING SEGREGATED LIST ALLOCATOR INTERNAL TEST SUITE....\n");
	debug("-------------------------------------------------------------\n");

	debug("TESTING: Alloc with First Fit as Default\n");
	test_seg_alloc();	
	debug("\n");

	debug("TESTING: Segragated List\n");
	test_seg_list();
	debug("\n");

	debug("TESTING: Copy\n");
	test_seg_copy();	
	debug("\n");

	debug("TESTING: Realloc\n");
	test_seg_realloc();	
	debug("\n");
}

inline void test_seg_alloc()
{
	size_t size = 4194304;
	SegAllocator allocator;

	// Test 1: Should alloc good
	debug("\tTest 1: Should alloc good\n");
	allocator.initialize_allocator(size);
	allocator.search_mode = (SEARCH_MODE_FIRST_FIT);


	void *p = nullptr;
	assert(p = allocator.alloc(419430)); // good
	header_t *ph = (header_t*)mem2header(p);
	assert(header_used(ph) == 1); // 1 is used, 0 is unused
	allocator.reset();

	// Test 2: Exact size, should pass 
	debug("\tTest 2: Exact size, should fail \n");
	assert(allocator.alloc(size) == nullptr); // bad
	allocator.reset();

	// // Test 3: Greater size, should fail 
	debug("\tTest 3: Greater size, should fail \n");
	assert(allocator.alloc(4194305) == nullptr); // bad
	allocator.reset();

	// // Test 4: Multiple allocs within size
	debug("\tTest 4: Multiple allocs within size\n");
	allocator.initialize_allocator(size);
	assert(allocator.alloc(1) != nullptr); // good
	assert(allocator.alloc(12) != nullptr); // good
	assert(allocator.alloc(17) != nullptr); // good
	assert(allocator.alloc(25) != nullptr); // good
	assert(allocator.alloc(33) != nullptr); // good
	assert(allocator.alloc(1) != nullptr); // good
	assert(allocator.alloc(1) != nullptr); // good
	assert(allocator.alloc(1) != nullptr); // good
	assert(allocator.alloc(1) != nullptr); // good
	allocator.reset();
	
	// // Test 5: Multiple allocs that exceed size
	debug("\tTest 5: Multiple allocs that exceed size\n");
	allocator.initialize_allocator(size);
	assert(allocator.alloc(1) != nullptr); // good
	assert(allocator.alloc(size) == nullptr); // bad
	allocator.reset();

	// // Test 6: Header
	debug("\tTest 6: Header\n");	
	void *d1 = allocator.alloc(3);
	header_t* d1h = (header_t*)mem2header(d1);
	assert(data_size(d1h) == BLOCK_SIZE);
	
	void *d2 = allocator.alloc(12);
	header_t* d2h = (header_t*)mem2header(d2);
	assert(data_size(d2h) == 16);
	
	// make sure d1 has not been overwritten
	d1h = (header_t*)mem2header(d1);
	assert(data_size(d1h) == 8);

	debug("\tTest 7: Free an object\n");
	allocator.free(d1);
	assert(header_used(d1h) == 0);
	// assert(allocator.heap.free_list_size() == 1);
	assert(data_size(d2h) == 16);

	// debug("\tTest 8: Re-allocate an object after first has been freed\n");
	// void *d3 = allocator.alloc(8);
	// header_t *d3h = (header_t*)mem2header(d3);
	// assert(d1h == d3h);
	// // assert(allocator.heap.free_list_size() == 0);

	// allocator.free(d2);
	// // assert(allocator.heap.free_list_size() == 1);

	// assert(header_used(d2h) == 0);
	// void *d4 = allocator.alloc(16);
	// // assert(allocator.heap.free_list_size() == 0);
	// header_t *d4h = (header_t*)mem2header(d4);
	// assert(d2h == d4h);
	// assert(header_used(d4h) == 1);

	allocator.free_allocator();
}

inline void test_seg_list()
{
	debug("\tTest 1: Alloc then free then alloc same size.\n");
	int size = 4194304; // 4mb
	SegAllocator allocator;

	allocator.initialize_allocator(size);

	void *p1 = allocator.alloc(8);
	allocator.free(p1);

	// 8 byte allocation should go into the first bin
	assert(allocator.seg_list_size(allocator.seg_list[0]) == 1);
	p1 = allocator.alloc(8);
	assert(allocator.seg_list_size(allocator.seg_list[0]) == 0);

	debug("\tTest 2: Alloc another block and then free 2 without merging.\n");
	void *p2 = allocator.alloc(8);
	void *p3 = allocator.alloc(8);
	allocator.free(p1);
	assert(allocator.seg_list_size(allocator.seg_list[0]) == 1);
	allocator.free(p3);
	assert(allocator.seg_list_size(allocator.seg_list[0]) == 2);
	
	debug("\tTest 3: Free the 4th header, triggering a merge between the 3rd and 4th headers.\n");
	void *p4 = allocator.alloc(8);
	allocator.free(p4);
	assert(allocator.seg_list_size(allocator.seg_list[0]) == 2);

	debug("\tTest 4: Free the 2nd header, triggering a merge between between all headers.\n");
	allocator.free(p2);
	assert(allocator.seg_list_size(allocator.seg_list[0]) == 1);

	// RESET
	allocator.reset();

	debug("\tTest 5: Split test: Alloc 128 bytes, no split with 128 byte alloc.\n");
	
	p1 = allocator.alloc(128);
	allocator.free(p1);

	// 128 byte allocation should in the 5th bin
	assert(allocator.seg_list_size(allocator.seg_list[4]) == 1);
	assert(data_size(allocator.seg_list[4]) == 128);

	p2 = allocator.alloc(128);
	header_t *p2h = mem2header(p2);
	assert(data_size(p2h) == 128);
	assert(allocator.seg_list_size(allocator.seg_list[4]) == 0);

	debug("\tTest 6: Split test: Alloc 128 bytes, no split with 120 byte alloc.\n");
	
	allocator.free(p2);
	assert(allocator.seg_list_size(allocator.seg_list[4]) == 1);
	assert(data_size(allocator.seg_list[4]) == 128);

	p2 = allocator.alloc(120);
	p2h = mem2header(p2);
	assert(data_size(p2h) == 128);
	assert(allocator.seg_list_size(allocator.seg_list[4]) == 0);

	debug("\tTest 7: Split test: Alloc 128 bytes, no split with 112 byte alloc.\n");
	
	allocator.free(p2);
	assert(allocator.seg_list_size(allocator.seg_list[4]) == 1);
	assert(data_size(allocator.seg_list[4]) == 128);

	p2 = allocator.alloc(112);
	p2h = mem2header(p2);
	assert(data_size(p2h) == 128);
	assert(allocator.seg_list_size(allocator.seg_list[4]) == 0);

	debug("\tTest 8: Split test: Alloc 128 bytes, no split with 104 byte alloc.\n");	
	
	allocator.free(p2);
	assert(allocator.seg_list_size(allocator.seg_list[4]) == 1);
	assert(data_size(allocator.seg_list[4]) == 128);

	p2 = allocator.alloc(104);
	p2h = mem2header(p2);
	assert(data_size(p2h) == 128);
	assert(allocator.seg_list_size(allocator.seg_list[4]) == 0);

	debug("\tTest 9: Split test: Alloc 128 bytes, split with 96 byte alloc.\n");
	
	allocator.free(p2);
	assert(allocator.seg_list_size(allocator.seg_list[4]) == 1);
	assert(data_size(allocator.seg_list[4]) == 128);
	
	p2 = allocator.alloc(96);
	p2h = mem2header(p2);
	assert(data_size(p2h) == 96);
	assert(allocator.seg_list_size(allocator.seg_list[2]) == 1);
	assert(data_size(allocator.seg_list[2]) == 24);

	allocator.free(p2);
	assert(allocator.seg_list_size(allocator.seg_list[4]) == 1);
	assert(data_size(allocator.seg_list[4]) == 96);

	allocator.free_allocator();
}

inline void test_seg_copy()
{
	int size = 4194304; // 4mb

	SegAllocator allocator;
	allocator.initialize_allocator(size);
	allocator.search_mode = (SEARCH_MODE_FIRST_FIT);

	// Test 1: Should alloc good
	debug("\tTest 1: Allocate an array of 5 ints and copy it to another array of same size.\n");

	// an array of 5 ints
	int *ptr = (int*)allocator.alloc(5 * sizeof(int));
	ptr[0] = 0;
	ptr[1] = 3;
	ptr[2] = 5;
	ptr[3] = 6;
	ptr[4] = 9;

	int *cpy = (int*)allocator.alloc(5 * sizeof(int));
	allocator.copy(cpy, ptr, 5*sizeof(int));

	assert(ptr[0] == cpy[0]);
	assert(ptr[1] == cpy[1]);
	assert(ptr[2] == cpy[2]);
	assert(ptr[3] == cpy[3]);
	assert(ptr[4] == cpy[4]);

	allocator.free_allocator();
}

inline void test_seg_realloc()
{
	int size = 4194304; // 4mb
	SegAllocator allocator;
	allocator.initialize_allocator(size);
	allocator.search_mode = (SEARCH_MODE_FIRST_FIT);

	// Test 1: Should alloc good
	debug("\tTest 1: Allocate an array of 5 ints and realloc it to an array of 10 ints.\n");

	int *ptr = (int*)allocator.alloc(5 * sizeof(int));
	ptr[0] = 0;
	ptr[1] = 3;
	ptr[2] = 5;
	ptr[3] = 6;
	ptr[4] = 9;

	int *new_alloc = (int*)allocator.realloc(ptr, 2*5*sizeof(int));
	assert(data_size(mem2header(new_alloc)) == 2*5*sizeof(int));
	assert(new_alloc[0] == 0);
	assert(new_alloc[1] == 3);
	assert(new_alloc[2] == 5);
	assert(new_alloc[3] == 6);
	assert(new_alloc[4] == 9);

	allocator.free_allocator();
}

#endif // DEBUG