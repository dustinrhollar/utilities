
#ifndef MAPLE_ALLOCATOR_H
#define MAPLE_ALLOCATOR_H

/* 
TODO(Dustin):
- Benchmarking
    - General Runtime
    - Internal fragmentation
    - Cache misses
- Better testing. Currently embedded in every instance of Allocator
- Red-Black Tree for the Free List to speed up allocations/de-allocations
- Buddy Allocator
- Slab Allocator
*/

#include <stdlib.h>

#include "utils.h"
#include "benchmark.h"

#include <stdlib.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>

// Alignment changes based on OS
#if defined(__x86_64__)
#define BLOCK_SIZE 8
#elif defined(__386__) || defined(__i386__) || defined(__DJGPP__)
#define BLOCK_SIZE 4
#else
#error Unsupported operating system.
#endif

struct Header 
{
	size_t flags; // size + used. Used is lower bit. 1 if used. 0 if free
	Header  *prev;  // List pointers used only if free  
	Header  *next;
};
typedef Header header_t;

#define HEADER_SIZE sizeof(header_t)

// Properly align a number to be aligned with a word type
// param:  size_t
// return: aligned size_t
#define ALIGN(n) (((n) + BLOCK_SIZE - 1) & ~(BLOCK_SIZE - 1))

// 0 is unused, 1 is used
// #define header_toggle_used(b) (b)->flags ^= 1 
#define header_toggle_0(b) ((b)->flags &= ~1L)
#define header_toggle_1(b) ((b)->flags |= 1L)
#define header_used(b)     (((b)->flags) & 1L)

#define data_size(b) (((b)->flags & ~1L) >> 1)
#define data_set_size(b, s) (b)->flags = (header_used(b) | ((s) << 1))

#define MIN_HEADER_SIZE  BLOCK_SIZE
#define MIN_LINK_SIZE    sizeof(Header*)

#define mem2header(d) ((header_t*)((char*)(d) - MIN_HEADER_SIZE))
#define header2mem(h) ((void*)((char*)h + MIN_HEADER_SIZE))

// Mininum alloc size will be the size of the header
// plus the minimum block size. This is used for 
// splitting regions of memory
#define MIN_SPLIT_SIZE (HEADER_SIZE + BLOCK_SIZE)
#define can_split(h, s) (data_size(h) >= ((s) + MIN_SPLIT_SIZE))

enum SearchMode
{
	SEARCH_MODE_FIRST_FIT,
	SEARCH_MODE_NEXT_FIT,
	SEARCH_MODE_BEST_FIT,
};

struct Arena
{
	size_t arena_size = 0;
	void *arena = nullptr;
	char *_brkp = nullptr; // program break
	char *_endp = nullptr;

	inline void initialize(size_t size);
	inline void free();

	inline void *_sbrk(size_t increment);
	inline void *request_memory(size_t size);
};

struct Heap
{
	SearchMode search_mode;
	header_t *last_access = nullptr; /* Used only for Best Fir Search */
	header_t *free_list   = nullptr;

	inline void initialize(SearchMode search_mode = SEARCH_MODE_FIRST_FIT);
	inline void free();

	inline void coalesce(header_t *left_header, header_t *right_header);
	inline header_t *split(header_t *header, size_t size);

	// free list data structure
	inline size_t free_list_size();
	inline void free_list_add(header_t *header);
	inline void free_list_remove(header_t *header);

	// Heap search methods
	inline header_t *find_free_header(size_t size);
	inline header_t *first_fit(size_t size);
	inline header_t *next_fit(size_t size);
	inline header_t *best_fit(size_t size);
};

// Allocator Interface
struct Allocator
{
	virtual void initialize_allocator(size_t size, SearchMode search_mode) = 0;
	virtual void free_allocator() = 0;
	virtual void reset() = 0;

	virtual void *alloc(size_t size) = 0;
	virtual void *realloc(void *ptr, size_t size) = 0;
	virtual void copy(void *dst, void *src, size_t size) = 0;
	virtual void free(void *data) = 0;
};

struct FreeListAllocator : public Allocator
{
	Arena arena;
	Heap heap;

	// Create the memory arena
	virtual void initialize_allocator(size_t size, SearchMode search_mode = SEARCH_MODE_FIRST_FIT) override;
	virtual void reset()  override;
	virtual void free_allocator() override;

	virtual void *alloc(size_t size) override;
	/*
	If the size is less than the current size of the allocation, a new region is not allocated.

	Re-allocates a block of memory with the provided size and copies the data from the old region
	to the new one. The old region is then freed and the new region is returned.
	*/
	virtual void *realloc(void *ptr, size_t size) override;
	virtual void copy(void *dst, void *src, size_t size) override;
	virtual void free(void *data) override;
};

// Segregated List Allocator
struct SegAllocator : public Allocator
{
	// Memory Arena Implementation
	size_t arena_size = 0;
	void *arena = nullptr;
	char *_brkp = nullptr; // program break
	char *_endp = nullptr;

	// Segragated List Heap information
	//---------------------------------------------------------------------------------------
	// 8 16 32 64 128 256 anything else
	SearchMode search_mode;
	header_t *seg_list[7];

	// Interface functions
	//---------------------------------------------------------------------------------------
	virtual void initialize_allocator(size_t size, SearchMode search_mode = SEARCH_MODE_FIRST_FIT) override;
	virtual void reset() override;
	virtual void free_allocator() override;

	virtual void *alloc(size_t size) override;
	/*
	If the size is less than the current size of the allocation, a new region is not allocated.

	Re-allocates a block of memory with the provided size and copies the data from the old region
	to the new one. The old region is then freed and the new region is returned.
	*/
	virtual void *realloc(void *ptr, size_t size) override;
	virtual void copy(void *dst, void *src, size_t size) override;
	virtual void free(void *data) override;

	// Internal Functions
	//---------------------------------------------------------------------------------------
	inline void *_sbrk(size_t increment);
	inline void *request_memory(size_t size);

	inline header_t *find_free_header(size_t size);
	inline header_t *first_fit(header_t *list_head, size_t size);

	inline void coalesce(header_t *left_header, header_t *right_header);
	inline header_t *split(header_t *header, size_t size);

	// seg list data structure
	inline size_t seg_list_size(header_t *list_head); // index for a bin in the seg list
	inline void seg_list_add(header_t **list_head, header_t *header);
	inline void seg_list_remove(header_t **list_head, header_t **header);
};

void free_list_internal_tests();
void segregated_list_internal_tests();

void run_free_list_benchmark(size_t num_operations, char *filename = nullptr);
void run_segragated_list_benchmark(size_t num_operations);

#endif // MAPLE_ALLOCATOR_H