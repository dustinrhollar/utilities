#ifndef C_ALLOCATOR_H
#define C_ALLOCATOR_H

struct CAllocator
{
	void *alloc(size_t size);
	void free(void *data);
};

void run_c_alloc_benchmark(size_t num_operations, char *filename = nullptr);

#endif