#include "red_black_tree.h"

static void bst_insert(RBNode *root, RBNode *x);
static void left_rotate(RBNode *root, RBNode *x);
static void right_rotate(RBNode *root, RBNode *x);

static void bst_insert(RBNode *root, RBNode *x)
{
	RBNode *y = nullptr;
	RBNode *iter = root;

	while (iter != nullptr)
	{
		y = iter;

		if (x->key < iter->key)
		{
			iter = iter->left;
		}
		else
		{
			iter = iter->right;
		}
	}

	if (y == nullptr)
	{
		root = x;
	}
	else
	{
		if (x->key < y->key)
		{

		}
	}
}

/*
Psuedocode:

node y;
y = x->right

// turn y's left sub-tree into x's right subtree
x->right = y->left
if (y->left != NULL) {
	y->left->parent = x;
}

// y's new parent was x's parent
y->parent = x->parent

// set the parent to point to y instead of x
if (x->parent == NULL) root = y
else {
	if (x == (x->parent)->left) {
		// x was on the left of its parent
		x->parent->left = y;
	}
	else {
		// x must have been on the right
		x->parent->right = y;
	}
}

// finally put x on y's left
y->left = x
x->parent = y

*/
static void left_rotate(RBNode *root, RBNode *x)
{
	RBNode *y = x->left;

	// turn y's left sub-tree into x's right subtree
	x->right = y->left;
	if (y->left != nullptr)
	{
		y->left->parent = x;
	}

	// y's new parent is x's old parent
	y->parent = x->parent;

	if (x->parent == nullptr) root = y;
	else 
	{
		if (x == (x->parent)->left)
		{ // x was the left node of the parent
			x->parent->left = y;
		}
		else
		{ // x must be the right node of the parent
			x->parent->right = y;
		}
	}

	// finally put x on y's left
	y->left = x;
	x->parent = y;
}

/*
Psuedocode:

node y;
y = x->left

// turn y's subtree into x's left 
x->left = y->right
if (y->right != NULL) {
	y->right->parent = x;
}


// y's new parent is x's old parent
y->parent = x->parent

if (x->parent == NULL) root = y
else {
	if (x == (x->parent)->left) {
		// x was on the left of its parent
		x->parent->left = y;
	}
	else {
		// x must have been on the right
		x->parent->right = y;
	}
}

// put x on y's right
y->right = x
x->parent = y


*/
static void right_rotate(RBNode *root, RBNode *x)
{
	RBNode *y = x->left;

	// turn y's right subtree into x's left
	x->left = y->right;
	if (y->right != nullptr)
	{
		y->right->parent = x;
	}

	// y's new parent is x's old one
	y->parent = x->parent;
	if (x->parent == nullptr) root = y;
	else
	{
		if (x == (x->parent)->left)
		{ // x was the left node of the parent
			x->parent->left = y;
		}
		else
		{ // x must have been the right node of the parent
			x->parent->right = y;
		}
	}

	// put x on y's right
	y->right = x;
	x->parent = y;
}


void initialize_rb_tree(RBNode *root, size_t key)
{
	root->parent = nullptr;
	root->left = nullptr;
	root->right = nullptr;

	root->color = RB_COLOR_BLACK;
	root->key = key;
}

void insert(RBNode *root, RBNode *x)
{
	if (root == nullptr)
	{
		root = x;
		x->color = RB_COLOR_BLACK;

		return;
	}

	// insert as you would in a bst tree
	bst_insert(root, x);

	// restore the red-black property
	x->color = RB_COLOR_RED;
	while ((x != root) && (x->parent->color == RB_COLOR_RED))
	{
		if (x->parent == x->parent->parent->left)
		{ // if x's parent is to the left, then y is x's right 'uncle'
			RBNode *y = x->parent->parent->right;

			if (y && 
				y->color == RB_COLOR_RED)
			{
				// Case 1: Change the colors
				x->parent->color = RB_COLOR_BLACK;
				y->color = RB_COLOR_BLACK;
				x->parent->parent->color = RB_COLOR_RED;
				// move x up the tree
				x = x->parent->parent;
			}
			else
			{ // y is a black node
				if (x == x->parent->right)
				{ // x is the right node of the parent
					// Case 2: Move x up and rotate
					x = x->parent;
					left_rotate(root, x);
				}
				else
				{ // x is the left node of the parent
					// Case 3: 
					x->parent->color = RB_COLOR_BLACK;
					x->parent->parent->color = RB_COLOR_RED;
					right_rotate(root, x->parent->parent);
				}
			}
		}
		else
		{ // x's parent is a right node, so y is x's left 'uncle'
			RBNode *y = x->parent->parent->left;

			if (y && y->color == RB_COLOR_RED)
			{
				// Case 1: Change the colors;
				x->parent->color = RB_COLOR_BLACK;
				y->color = RB_COLOR_BLACK;
				x->parent->parent->color = RB_COLOR_RED;
				// move x up the tree
				x = x->parent->parent;
			}
			else
			{ // y is a black node
				if (x == x->parent->left)
				{ // x is the right node of the parent
					// Case 2: Move x up and rotate
					x = x->parent;
					right_rotate(root, x);
				}
				else
				{ // x is the left node of the parent
					// Case 3: 
					x->parent->color = RB_COLOR_BLACK;
					x->parent->parent->color = RB_COLOR_RED;
					left_rotate(root, x->parent->parent);
				}
			}
		}
	}
}


void remove(RBNode *root, RBNode x)
{

}