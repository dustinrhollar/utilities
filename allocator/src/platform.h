#ifndef MAPLE_PLATFORM_H
#define MAPLE_PLATFORM_H

// Timers
#define Time         platform_time
#define set_timer    platform_set_timer
#define elapsed_time platform_elapsed_time

// OS Specifc Memory Allocation/Free
#define os_alloc_memory platform_os_alloc_memory
#define os_free_memory  platform_os_free_memory

#ifdef __unix__ 

#include <time.h>
#define platform_time timespec
#define platform_set_timer(t) (clock_gettime(CLOCK_REALTIME, (timespec*)(t)))
double platform_elapsed_time(platform_time *start, platform_time *end);

#include <sys/mman.h> 
#define platform_os_alloc_memory(s)  mmap(0, (s), PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0)
#define platform_os_free_memory(p,s) munmap((p), (s))

#else
#error Operating system not supported!
#endif




#endif // MAPLE_PLATFORM_H