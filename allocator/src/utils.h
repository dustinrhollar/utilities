#ifndef MAPLE_UTILS_H
#define MAPLE_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>

static inline void
debug(char *fmt, ...)
{
#ifdef DEBUG
	va_list my_args;
	va_start(my_args, fmt);
	vfprintf(stderr, fmt, my_args);
	va_end(my_args);
#endif
}

#endif // MAPLE_UTILS_H