#include "utils.h"
#include "platform.h"
#include "benchmark.h"
#include "c_allocator.h"

#include <stdlib.h>

void *CAllocator::alloc(size_t size)
{
	return malloc(size);	
}
void CAllocator::free(void *data)
{
	free(data);
}

BENCHMARK_SINGLE_ALLOCATION(c_alloc_single_allocation)
{
	CAllocator *a = (CAllocator*)allocator;
	Time start;
	Time end;

	set_timer(&start);

	unsigned int op = 0;
	while (op < num_operations)
	{
		a->alloc(size);
		++op;
	}
	set_timer(&end);

	double et = elapsed_time(&start, &end);
	BenchmarkResults results = build_benchmark_results(num_operations, et, 0);
	// print_benchmark_results(&results);
	write_benchmark_file(fp, &results);
}

BENCHMARK_SINGLE_FREE(c_alloc_single_free)
{
	CAllocator *a = (CAllocator*)allocator;
	Time start;
	Time end;

	int **ptr_array = (int**)malloc(num_operations * sizeof(int*));

	unsigned int op = 0;
	while (op < num_operations)
	{
		*(ptr_array + op) = (int*)malloc(size);
		++op;
	}

	set_timer(&start);

	op = 0;
	while (op < num_operations)
	{
		free(*(ptr_array + op));
		++op;
	}

	set_timer(&end);

	double et = elapsed_time(&start, &end);
	BenchmarkResults results = build_benchmark_results(num_operations, et, 0);
	// print_benchmark_results(&results);
	write_benchmark_file(fp, &results);
}

BENCHMARK_MULTIPLE_ALLOCATION(c_alloc_multiple_allocation)
{
	for (size_t i = 0; i < size_count; ++i)
	{
		char buffer[128];
		sprintf(buffer, "benchmarks/c_alloc_benchmark_%ld.csv", sizes[i]);

		FILE *file = fopen(buffer, "w");
		write_benchmark_header(file);

		debug("\tMultiple Allcoations: %ldbyte\n", sizes[i]);

		for (int op = 0; op < num_operations; ++op)
			c_alloc_single_allocation(file, allocator, sizes[i], op);
	}	
}

BENCHMARK_MULTIPLE_FREE(c_alloc_multiple_free)
{
	for (size_t size = 0; size < size_count; ++size)
	{
		// We want to write to a file for each size of benchmark
		char buffer[128];
		sprintf(buffer, "benchmarks/c_alloc_free_benchmark_%ld.csv", sizes[size]);

		FILE *file = fopen(buffer, "w");
		write_benchmark_header(file);

		debug("\tMultiple Frees: %ldbyte\n", sizes[size]);

		for (int op = 0; op < num_operations; ++op)
			c_alloc_single_free(file, allocator, sizes[size], op);

		fclose(file);
	}
}

// void c_multiple_allocation_benchmarks(size_t num_operations)
// {
// 	CAllocator allocator;

// 	size_t allocation_size[] = {32, 64, 256, 512, 1024, 2048, 4096};

// 	FILE *fp = fopen("benchmarks/c_alloc_benchmark_32.csv", "w");
// 	write_benchmark_header(fp);

// 	debug("  Multiple Allocations: 32byte\n");
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		c_alloc_multiple_allocation(fp, (void*)&allocator, &allocation_size[0], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 64byte\n");

// 	fp = fopen("benchmarks/c_alloc_benchmark_64.csv", "w");
// 	write_benchmark_header(fp);	

// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		c_alloc_multiple_allocation(fp, (void*)&allocator, &allocation_size[1], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 256byte\n");
	
// 	fp = fopen("benchmarks/c_alloc_benchmark_256.csv", "w");
// 	write_benchmark_header(fp);	
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		c_alloc_multiple_allocation(fp, (void*)&allocator, &allocation_size[2], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 512byte\n");

// 	fp = fopen("benchmarks/c_alloc_benchmark_512.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		c_alloc_multiple_allocation(fp, (void*)&allocator, &allocation_size[3], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 1024byte\n");

// 	fp = fopen("benchmarks/c_alloc_benchmark_1024.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		c_alloc_multiple_allocation(fp, (void*)&allocator, &allocation_size[4], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 2048byte\n");

// 	fp = fopen("benchmarks/c_alloc_benchmark_2048.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		c_alloc_multiple_allocation(fp, (void*)&allocator, &allocation_size[5], 1, op+1);
// 	}

// 	fclose(fp);

// 	debug("  Multiple Allocations: 4096byte\n");

// 	fp = fopen("benchmarks/c_alloc_benchmark_4096.csv", "w");
// 	write_benchmark_header(fp);
	
// 	for (size_t op = 0; op < num_operations; op += 1)
// 	{
// 		c_alloc_multiple_allocation(fp, (void*)&allocator, &allocation_size[6], 1, op+1);
// 	}

// 	fclose(fp);
// }

void run_c_alloc_benchmark(size_t num_operations, char *filename)
{
	debug("C ALLOCATOR\n");

	CAllocator allocator;

	size_t allocation_size[] = {32, 64, 256, 512, 1024, 2048, 4096};

	c_alloc_multiple_allocation(&allocator, allocation_size, 7, num_operations);
	c_alloc_multiple_free(&allocator, allocation_size, 7, num_operations);
}
