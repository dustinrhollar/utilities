/*
This implementation of a dynamic array is modeled after Sean Barrett's implementation.
Barrett's full implementation can be found at: https://github.com/nothings/stb/blob/master/stb_ds.h

Usage:
#define MAPLE_DYN_ARRAY_IMPLEMENTATION
#include "dyn_array.h"

Optionally, you can specify the memory management. By default, stdlib malloc and free are used.
#define maple_dyn_alloc(allocator, size)
#define maple_dyn_free(allocator, ptr)

---------------------------------------------------------
* To set the allocator used by the array:
* @param ptr       pointer to the array
* @param allocator pointer to the memory allocator that is stored in the header

arr_set_allocator(E *ptr, T *allocator)

---------------------------------------------------------
* Place an element at the end of the array. Note that it is possible for the array to
* when inserting an element, so the new pointer is returned. 
* @param ptr     pointer to the array
* @param element pointer to the element to insert into the array
* @return a pointer to the start of the array

arr_put(E *ptr, E *element)

---------------------------------------------------------

*/

#ifndef MAPLE_DYN_ARRAY
#define MAPLE_DYN_ARRAY

// #ifdef MAPLE_DYN_ARRAY_IMPLEMENTATION 

#if defined(maple_dyn_alloc) && (!defined(maple_dyn_free) || !defined(maple_dyn_realloc)) || defined(maple_dyn_free) && (!defined(maple_dyn_alloc) || !defined(maple_dyn_realloc))
#error You must define maple_alloc, maple_realloc, and maple_free in order to use maple_dyn_array custom memory allocation!
#endif

#if !defined(maple_dyn_alloc) && !defined(maple_dyn_free) && !defined(maple_dyn_realloc)
#include <stdlib.h>
#define maple_dyn_alloc(a, s)      malloc(s)
#define maple_dyn_free(a, p)       free(p) 
#define maple_dyn_realloc(a, p, n) realloc(p, n)
#endif 

#define DYN_ARRAY_DEFAULT_SIZE 10

#define arr_initialize    maple_arr_initialize
#define arr_allocator     maple_allocator
#define arr_set_allocator maple_set_allocator
#define arr_len           maple_arr_len
#define arr_put           maple_arr_put
#define arr_cap           maple_arr_cap
#define arr_set_cap       maple_arr_set_cap
#define arr_pop           maple_arr_pop
#define arr_free          maple_arr_free

struct DynArrayHeader
{
	void   *a = NULL; // memory allocator. set to null by default 
	size_t cap;
	size_t len;
};

#define DYN_HEADER_SIZE sizeof(DynArrayHeader)
#define dyn_header2mem(h) ((void*)((char*)(h) + DYN_HEADER_SIZE))
#define mem2dyn_header(p) ((DynArrayHeader*)((char*)(p) - DYN_HEADER_SIZE))

#define maple_arr_cap(p)          ((p) ? mem2dyn_header(p)->cap : 0)
#define maple_arr_len(p)          ((p) ? mem2dyn_header(p)->len : 0)

#define maple_initialize(p,a,n,c) (maple_initializef_wrapper((p),(a),(n),(c)))
#define maple_should_grow(p,n) ((!(p) || (maple_arr_len(p) + n > maple_arr_cap(p))) ? (maple_grow(p,n,0), 0) : 0)
#define maple_grow(p,n,c) ((p) = maple_growf_wrapper((p), sizeof *(p),(n),(c))) 

#define maple_arr_initialize(p,a,n,c) (maple_initialize((p),a,n,c))
#define maple_allocator(p)        (mem2dyn_header(p)->a)           
#define maple_set_allocator(p, a) (mem2dyn_header(p)->a = (a))
#define maple_arr_put(p, e)       (maple_should_grow(p,1), ((p)[mem2dyn_header(p)->len++]) = e)
#define maple_arr_set_cap(p,c)    (maple_growf_wrapper(p,0,c))
#define maple_arr_pop(p)          (((p) && maple_arr_len(p)>0) ? --mem2dyn_header(p)->len, (p)[mem2dyn_header(p)->len] : NULL)
#define maple_arr_free(p)         ((void)((p) ? maple_dyn_free(maple_allocator(p), p) : (void)0), (p)=nullptr)

void *maple_initializef(void *ptr, void *allocator, size_t size_per_element, size_t cap);
void *maple_growf(void *ptr, size_t size_per_element, size_t add_len, size_t min_cap);

#ifdef __cplusplus
// When compiling for C++, you have to use templates to get the T* to cast to and from a void*
template<class T, class U> static T * maple_initializef_wrapper(T *ptr, U *allocator, size_t size_per_element, size_t cap) {
  return (T*)maple_initializef((void *)ptr, (void *)allocator, size_per_element, cap);
}

template<class T> static T * maple_growf_wrapper(T *ptr, size_t size_per_element, size_t add_len, size_t min_cap) {
  return (T*)maple_growf((void *)ptr, size_per_element, add_len, min_cap);
}

#else

#define maple_initializef_wrapper maple_initializef
#define maple_growf_wrapper       maple_growf

#endif

// #endif // MAPLE_DYN_ARRAY_IMPLEMENTATION

// #ifdef MAPLE_CLASS_DYN_IMPLEMENTATION

#include <allocator.h>
#include <string.h>

template<class T>
class DynArray
{
public:
	// member variables
	size_t len;
	size_t cap;
	T *ptr = nullptr;
	Allocator *allocator = nullptr;

	// Constructors
	//------------------------------------------------------------------------------------------------
	DynArray(size_t _cap, Allocator *_allocator = nullptr, T *cpy = nullptr, size_t cpy_count = 0) 
	{
		cap = _cap;

		if (_allocator)
		{
			allocator = _allocator;
		} 

		if (cpy_count > 0 && cpy)
		{
			// If the cpy array was larger than the request cap,
			// adjust the cap accordingly
			len = cpy_count;
			if (len > cap)
			{
				cap = cpy_count * 2;
			}

			if (allocator)
			{
				allocator->alloc(sizeof(T) * cap);
			}
			else
			{
				malloc(sizeof(T) * cap);
			}

			for (size_t idx = 0; idx < cpy_count; ++idx)
				*(ptr + idx) = *(cpy + idx);
		}
	}

	~DynArray()
	{
		if (ptr)
		{
			if (allocator)
			{
				allocator->free(ptr);	
			}
			else
				free(ptr);
			
			cap = 0;
			len = 0;
		}
	}


	// Member Functions
	//------------------------------------------------------------------------------------------------
	// grows the array by 2 twice its current capacity
	inline void grow()
	{
		cap *= 2;
		ptr = (allocator) ? allocator->realloc(ptr, cap) : realloc(ptr, cap);
	}

	// copy into the Array or copy by reference?
	// 
	void add(T &element, int idx) 
	{
		if (len + 1 >= cap)
			grow();

		for (int i = len; i > idx; ++i)
		{
			ptr[i] = ptr[i - 1];
		}

		ptr[len++] = element;
	}

	T* remove(int idx)
	{
		return nullptr;
	}

	// adds to the back of the array
	void push(T element)
	{
		if (len + 1 >= cap)
			grow();
		ptr[len++] = element;
	}

	// removes from the back of the array
	T* pop()
	{
		return nullptr;
	}
};

// #endif // MAPLE_CLASS_DYN_IMPLEMENTATION

#if defined(DEBUG)

void test_c_dyn_array();

#endif // DEBUG

#endif // MAPLE_DYN_ARRAY